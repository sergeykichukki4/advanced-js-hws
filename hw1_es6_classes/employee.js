"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get nameInfo() {
        return this._name;
    }

    set nameInfo(newName) {
        this._name = newName;
    }

    get ageInfo() {
        return this._age;
    }

    set ageInfo(newAge) {
        this._age = newAge;
    }

    get salaryInfo (){
        return this._salary;
    }

    set salaryInfo (newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(lang, ...args) {
        super(...args);
        this._lang = lang;
    }

    get langInfo () {
        return this._lang;
    }

    set langInfo (newArrayLang) {
        this._lang = newArrayLang;
    }
    get salaryInfo () {
        return this._salary * 3;
    }
}

const architectIvan = new Employee("Ivan", 38, 4000);
const pythonDeveloper = new Programmer( ["Python"], "Ivan", 27, 1000,);
const fullStackDeveloper = new Programmer(["Python","Javascript", "Node.js"], "Sergey", 31, 1400, );

console.log(architectIvan);
console.log(architectIvan.salaryInfo);
console.log(pythonDeveloper);
console.log(fullStackDeveloper);
console.log(fullStackDeveloper.salaryInfo);