const user1 = {
    name: "John",
    years: 30
};

const {name:firstName, years: age, isAdmin = false} = user1;

console.log(firstName, age, isAdmin)