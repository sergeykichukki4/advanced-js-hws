const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const fullInfoEmployee = {...employee, age: 30, salary: 400000};
console.log(fullInfoEmployee);