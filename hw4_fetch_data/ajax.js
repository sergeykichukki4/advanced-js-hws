'use strict';

const renderAllFilm = (url, parentNode, id, paramList, classListForElem = '') => {
    fetch(url)
        .then(response => response.json())
        .then(elem => {
            elem.forEach(film => {
                parentNode.append(renderFilmInfo(film, film[id], paramList, classListForElem))
            })
        })
}

const renderCharacterInfo = (url) => {
    return fetch(url)
        .then(response => response.json())
        .then(elem => renderObjInfo(elem, 'character'))
}

const renderObjInfo = (obj, classList = '', id = '', paramList = false) => {

    const infoList = document.createElement('ul');
    infoList.setAttribute('id', id)
    infoList.className = classList;

    paramList = !!paramList ? paramList : Object.keys(obj)

    for (const paramListElement of paramList) {
        const objParam = document.createElement('li');
        objParam.className = paramListElement
        objParam.textContent = `${paramListElement}: ${obj[paramListElement]}`
        infoList.append(objParam);
    }

    return infoList;
}

const renderFilmInfo = (film, id, paramList = false, classList = '') => {

    const infoList = renderObjInfo(film, classList, id, paramList)


    const charactersElem = document.createElement('li');
    charactersElem.textContent = 'characters:'

    infoList.append(charactersElem);


    const promiseList = film['characters'].map(character => renderCharacterInfo(character))

    Promise.all(promiseList).then((character) => {
        character.forEach(characterParam => {
            charactersElem.append(characterParam)
        })
    })

    return infoList;
}

renderAllFilm('https://ajax.test-danit.com/api/swapi/films', document.body, 'episodeId', ['episodeId', 'name', 'openingCrawl'], 'film');
