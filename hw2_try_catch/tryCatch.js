"use strict";
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const ulRoot = document.getElementById('root');

class NotAllBookProperties extends Error {
    constructor(missedPropertyName, missedPropertyValue) {
        super();
        this.name = 'NotAllBookProperties';
        this.message = `Not all properties to represent the book. The ${missedPropertyName} is ${missedPropertyValue}`;
    }
}

class Book {
    constructor(book) {
        // if (book &&
        //     book.author !== null &&
        //     book.author !== undefined) {
        //     this.author = book.author;
        // } else {
        //     throw new NotAllBookProperties('book.author', book.author);
        // }
        //
        // if (book.name !== null &&
        //     book.name !== undefined) {
        //     this.name = book.name;
        // } else {
        //     throw new NotAllBookProperties('book.name', book.name);
        // }
        //
        // if (book.price !== null &&
        //     book.price !== undefined) {
        //     this.price = book.price;
        // } else {
        //     throw new NotAllBookProperties('book.price', book.price);
        // }
        const checkAndAssignProperty = (target, source, propertyName) => {
            if (source[propertyName] !== null && source[propertyName] !== undefined) {
                target[propertyName] = source[propertyName];
            } else {
                throw new NotAllBookProperties(`book.${propertyName}`, source[propertyName]);
            }
        };

        if (book) {
            checkAndAssignProperty(this, book, 'author');
            checkAndAssignProperty(this, book, 'name');
            checkAndAssignProperty(this, book, 'price');
        }
        // if(book &&
        // book.author !== null &&
        // book.author !== undefined &&
        // book.name !== null &&
        // book.name !== undefined &&
        // book.price !== null &&
        // book.price !== undefined) {
        //     this.author = book.author;
        //     this.name = book.name;
        //     this.price = book.price;
        // } else {
        //     throw new NotAllBookProperties('qwe', undefined);
        // }
    }

    render(container) {
        container.insertAdjacentHTML('beforeend', `<li>author: ${this.author}
                        name: ${this.name}
                        price: ${this.price}</li>`);
    }
}

books.forEach(el => {
    try {
        new Book(el).render(ulRoot);
    } catch (err) {
        if (err.name === 'NotAllBookProperties') {
            console.log(err)
        } else {
            throw err;
        }
    }
})