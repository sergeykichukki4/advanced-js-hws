function showForm(title, textBtn) {
    const formContainer = createElement({type: 'div', classList: 'background'});

    formContainer.innerHTML =
        `<div class="background">
            <form class="add-post-form">
                <h3 class="form-title">${title}</h3>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title">
                </div>
                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" id="body" rows="3"></textarea>
                </div>
                <button class="btn submit" type="submit">${textBtn}</button>
                <button class="btn close">Cancel</button>
            </form>
        </div>`

    document.body.append(formContainer);

    formContainer.addEventListener('click', (event) => {
        const eventClassList = event.target.classList
        if (eventClassList.contains('background') || eventClassList.contains('close')) {
            formContainer.remove()
        }
    })

    return formContainer;
}